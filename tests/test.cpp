#include <iostream>
#include <vector>
#include <functional>

#include "../include/equation.h"
#include "../include/geneticalgorithmsettings.h"
#include "../include/population.h"
#include "../include/crossover.h"
#include "../include/mutation.h"
#include "../include/selection.h"
#include "../include/geneticalgorithm.h"

// =====================================================================
//                                 main
// =====================================================================

int main(void) {
    
        // ========================= Genetic Algorithm =========================
        
        GApp::GeneticAlgorithmSettings settings;
        settings.tournamentSize             = 5;
        settings.nElite                     = 5;
        settings.numberOfGenerations        = 2000;
        settings.seed                       = 0;
        settings.nPoint                     = 1;
        settings.mutationProbability        = 0.05;
        settings.width                      = 4.0;
        settings.randomGenerator.seed(settings.seed);
        
        // ============================ Population =============================
        
        // Create population
        std::size_t cap = 10;
        std::shared_ptr< GApp::Population<int> > popPtr = std::make_shared<GApp::Population<int>>(cap);
        
        // Fill population
        std::uniform_int_distribution<> dist(-100, 100);
        std::vector<int> c{1,4,2,3,5,3,3,1,10,43,13,64,32,1};
        int rhs = 4;
        for (std::size_t i=0; i<cap; i++) {
            popPtr->addIndividual(
                        Equation::getRandomEquationPtr(
                                    settings.randomGenerator, dist,
                                    c, rhs, false
                        )
                    );    
        }
        
        for (std::size_t i=0; i<cap; i++) {
            std::cout << popPtr->getIndividual(i)->fitness() << std::endl;
        }
        
        // ============================ Operators ==============================
        
//         std::function<void(std::shared_ptr< GApp::Population<int> >,
//                            std::size_t,
//                            std::vector<std::size_t> &,
//                            GApp::GeneticAlgorithmSettings &)> selectionFunction = GApp::Selection::tournamentSelection<int>;

        std::function<void(std::shared_ptr< GApp::Population<int> >,
                           std::size_t,
                           std::vector<std::size_t> &,
                           GApp::GeneticAlgorithmSettings &)> selectionFunction = GApp::Selection::rouletteWheelSelection<int>;

        std::function<void(std::size_t,
                           std::vector<int> &,
                           std::vector<int> &,
                           GApp::GeneticAlgorithmSettings &)> crossoverFunction = GApp::Crossover::npoint<int>;
                                                      
        std::function<void(int &,
                           GApp::GeneticAlgorithmSettings &)> mutationFunction = GApp::Mutation::intGaussian;
                           
        // ========================= Genetic Algorithm =========================
                           
        GApp::GeneticAlgorithm<int> ga(popPtr, settings, selectionFunction, crossoverFunction, mutationFunction);
  
        std::ofstream of;
        of.open("performance.dat");
        of << "# gen min avg max\n";
        
std::cout << "Start generations" << std::endl;
        
        for (std::size_t ii=0; ii<settings.numberOfGenerations; ii++) {
            std::cout << popPtr->highestFitness() << std::endl;
            of << ii << " " << popPtr->lowestFitness() << " " << popPtr->averageFitness() << " " << popPtr->highestFitness() << "\n";
            ga.advance();
        }
        
        of.close();
        
//         std::cout << "hiiiiiii" << std::endl;
//         
//         popPtr->updateRanking();
//         for (std::size_t ii=0; ii<popPtr->size(); ii++) {
//             std::cout << popPtr->getFittestIndividual(ii)->fitness() << " -> " << popPtr->getFittestIndividual(ii)->print() << std::endl;
//         }
        
        return 0;

}
