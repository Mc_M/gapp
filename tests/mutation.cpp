/**
 * @brief Test implementations from Mutation class.
 * 
 * Tests that check the pre-implemented mutation operators.
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <string>
#include "../include/mutation.h"
#include "../include/geneticalgorithmsettings.h"

// =====================================================================
//                                 main
// =====================================================================

int main(void) {
    
    // Test intGaussian: Draw a large number of values and check the distribution
    int value = 4;   // Mean value
    double width = 3.2; // Width
    std::size_t size = 100000;
    GApp::GeneticAlgorithmSettings settings;
    settings.width = width;
    std::vector<int> values;
    values.resize(size);
    for (std::size_t i=0; i<size; i++) {
        values[i] = value;
    }
    for (std::size_t i=0; i<size; i++) {
        GApp::Mutation::intGaussian(values[i], settings);
    }
    std::string fileName = "mean"+std::to_string(value)+"_width"+std::to_string(width)+".dat";
    std::ofstream f;
    f.open(fileName);
    f << value << "\n";
    f << width << "\n";
    for (auto el : values) {
        f << el << "\n";
    }
    f.close();

}
