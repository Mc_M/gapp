import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from scipy.stats import norm

# Load filenames that contain data
filenames = [val for val in os.listdir() if ".dat" in val]

# Load data and process it
for f in filenames:
    data = np.loadtxt(f)
    mean = data[0]
    width = data[1]
    data = data[2:].astype(int)
    nrNumbers = len(set(data)) # How many different numbers there are
    
    n, bins, patches = plt.hist(data, nrNumbers, normed=1, facecolor='green', alpha=0.75)
        
    # Fit a normal distribution to the data:
    mu, std = norm.fit(data)
    print("Fitted mean and width:")
    print("\t{} (correct: {})".format(mu, mean))
    print("\t{} (correct: {})".format(std, width))
    
    y = mlab.normpdf( bins, mean, width)
    l = plt.plot(bins, y, 'r--', linewidth=1, label="correct parameters: {}, {}".format(mean, width))
    
    y = mlab.normpdf( bins, mu, std)
    l = plt.plot(bins, y, 'b--', linewidth=1, label="fitted parameters: {}, {}".format(mu, std))
    
    plt.xlabel("x")
    plt.ylabel("normalized pdf")
    plt.legend()
    plt.show()
    
