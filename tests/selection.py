"""Read rouletteWheelSelection.dat to verify statistical properties."""

import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import argparse

# CLI
parser = argparse.ArgumentParser(description="Read rouletteWheelSelection.dat to verify statistical properties.")
parser.add_argument("path",
                    help="Path to rouletteWheelSelection.parentIndices.dat.")
parser.add_argument("p",
                    help="Path to rouletteWheelSelection.propabilities.dat.")
args                = parser.parse_args()
path                = args.path
p                   = args.p

# Read data
data = np.loadtxt(path)
minVal = data.min()
maxVal = data.max()

# Plot histogram (correct binning according to
# https://stackoverflow.com/questions/30112420/histogram-for-discrete-values-with-matplotlib)
d = np.diff(np.unique(data)).min()
left_of_first_bin = data.min() - float(d)/2
right_of_last_bin = data.max() + float(d)/2
plt.hist(data, np.arange(left_of_first_bin, right_of_last_bin + d, d),
         normed=True, label="selected parents")

# Plot expected propabilities
p = np.loadtxt(p)
plt.scatter(range(len(p)), p, c="red", zorder=100,
            label="expected propabilities")

# Settings
plt.xlabel("Individual")
plt.ylabel("normalized pdf")
plt.legend()
plt.show()
    
