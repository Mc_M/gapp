/*
 * Test for class Equation which implements Individual<int>
 * and will be used for tests concerning the actual genetic
 * routines and classes.
 */

#include <iostream>
#include <vector>
#include "../include/equation.h"
#include <cmath>
// #include <memory>
// #include <cstdlib>
// #include <string>
// #include <sstream>
// #include <fstream>
// #include <random>


// =====================================================================
//                                 main
// =====================================================================

int main(void) {
    
    // ------------------------ Build equation ---------------------------------
    
    std::vector<int> x{1,2,3,4};
    std::vector<int> c{4,3,2,1};
    int rhs = 42;
    bool verbose = false;
    
    Equation eq(x, c, rhs, verbose);
    
    // --------------------- Check Equation methods ----------------------------
    
    std::cout << "Equation <<: " << eq << std::endl;
    
    std::cout << "Equation::getX(): ";
    for (auto el : eq.getX()) {
        std::cout << el << " ";
    }
    std::cout << std::endl;
    
    std::cout << "Equation::getC(): ";
    for (auto el : eq.getC()) {
        std::cout << el << " ";
    }
    std::cout << std::endl;
    
    std::cout << "Equation::getRhs(): " << eq.getRhs() << std::endl;
    
    // ------------------- Check Individual interface --------------------------
    
    std::cout << "Equation::genomeLength(): " << eq.genomeLength() << std::endl;
    
    std::cout << "Equation::getGene(): ";
    for (std::size_t i=0; i<eq.genomeLength(); i++) {
        std::cout << eq.getGene(i) << " ";
    }
    std::cout << std::endl;
    
    std::cout << "Equation::print(): " << eq.print() << std::endl;
    
    std::cout << "Equation::setGene(): ";
    for (std::size_t i=0; i<eq.genomeLength(); i++) {
        eq.setGene(i, 2*i);
    }
    std::cout << eq.print() << std::endl;
    
    std::cout << "Equation::fitness(): " << eq.fitness() << " ";
    double f = 0.0;
    for (std::size_t i=0; i<eq.genomeLength(); i++) {
        f += c[i]*eq.getGene(i);
    }
    f -= eq.getRhs();
    f = std::abs(f);
    f = 1/(1+f);
    std::cout << "(manual: " << f << ")" << std::endl;
    
    // ------------------------ Build another equation -------------------------
    
    Equation eq2(x, c, rhs, true);
    
    std::cout << "Equation: Check verbose:" << std::endl;
    eq2.fitness();
    
    std::cout << "Equation counter: " << eq2 << std::endl;

}
