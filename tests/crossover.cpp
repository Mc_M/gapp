/**
 * @brief Test implementations from Crossover class.
 * 
 * Tests that check the pre-implemented mutation operators.
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <string>
#include "../include/crossover.h"
#include "../include/geneticalgorithmsettings.h"

// =====================================================================
//                                 main
// =====================================================================

int main(void) {
    
    // Test scope
    std::size_t genomeLength = 13;
    std::vector<std::size_t> nPoints{0, 1, 2, genomeLength-1};
    
    // Settings
    GApp::GeneticAlgorithmSettings settings;
    settings.verbose = 2;
    
    // Test
    for (auto nPoint : nPoints) {
        
        std::cout << "[ nPoint = " << nPoint << std::endl << std::endl;
        settings.nPoint = nPoint;
    
        // Fill test genomes
        std::vector<int> genome1, genome2;
        genome1.resize(genomeLength);
        genome2.resize(genomeLength);
        for (std::size_t i=0; i<genomeLength; i++) {
            genome1[i] = 1;
            genome2[i] = 2;
        }
    
        // Feedback before
        std::cout << "Before:" << std::endl;
        for (auto el : genome1) {
            std::cout << el << " ";
        }
        std::cout << std::endl;
        for (auto el : genome2) {
            std::cout << el << " ";
        }
        std::cout << std::endl << std::endl;
    
        // Operation
        GApp::Crossover::npoint<int>(genomeLength, genome1, genome2, settings);
    
        // Feedback after
        std::cout << "After:" << std::endl;
        for (auto el : genome1) {
            std::cout << el << " ";
        }
        std::cout << std::endl;
        for (auto el : genome2) {
            std::cout << el << " ";
        }
        std::cout << std::endl << std::endl;
    
        // Next iteration
        std::cout << "]" << std::endl << std::endl << std::endl;
    
    }

    return 0;

}
