/**
 * @brief Test implementations from Selection class.
 * 
 * Tests that check the pre-implemented selection operators.
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <string>

#include "../include/equation.h"
#include "../include/population.h"

#include "../include/geneticalgorithmsettings.h"
#include "../include/selection.h"

// =====================================================================
//                                 main
// =====================================================================

int main(void) {
    
    // ============== Build a population of Equation instances =================
    std::cout << "Build population and fill with random Equation's ... ";
    std::size_t cap = 8;
    std::size_t size = cap-2;
    std::shared_ptr<GApp::Population<int>> popPtr = std::make_shared<GApp::Population<int>>(cap, false);
    std::default_random_engine r(1337);
    std::size_t randomRange = 100;
    int rhs = 42;
    std::vector<int> c;
    std::size_t cSize = 4;
    c.resize(cSize);
    for (std::size_t i=0; i<cSize; i++) {
        c[i] = r() % (2*randomRange) - randomRange;
    }
    for (std::size_t i=0; i<size; i++) {
        std::vector<int> x;
        x.resize(cSize);
        for (std::size_t j=0; j<cSize; j++) {
            x[j] = r() % (2*randomRange) - randomRange;
        }
        std::shared_ptr<Equation> eqPtr = std::make_shared<Equation>(x, c, rhs, false);
        popPtr->addIndividual(eqPtr);
    }
    std::cout << "done" << std::endl << std::endl;
    std::cout << "Fitnesses:" << std::endl;
    for (std::size_t i=0; i<popPtr->size(); i++) {
        std::cout << "   " << i << " -> " << popPtr->getIndividual(i)->fitness() << std::endl;
    }
    std::cout << std::endl;
    
    // ========================== Selection settings ===========================
    std::size_t numberOfParents = 10;
    GApp::GeneticAlgorithmSettings settings;
    settings.verbose = 2;
    settings.tournamentSize = 4;
    
    // ================= Call selection: tournamentSelection ===================
    std::cout << "tournamentSelection():" << std::endl;
    std::vector<std::size_t> parentIndices;
    parentIndices.resize(numberOfParents);
    GApp::Selection::tournamentSelection<int>(popPtr, numberOfParents,
                                              parentIndices, settings);
    std::cout << "Chosen parent indices:" << std::endl;
    for (auto el : parentIndices) {
        std::cout << "   " << el << std::endl;
    }
    std::cout << std::endl;
    
    // ================ Call selection: rouletteWheelSelection =================
    std::cout << "rouletteWheelSelection():" << std::endl;
    GApp::Selection::rouletteWheelSelection<int>(popPtr, numberOfParents,
                                                 parentIndices, settings);
    std::cout << "Chosen parent indices:" << std::endl;
    for (auto el : parentIndices) {
        std::cout << "   " << el << std::endl;
    }
    std::vector<double> fitnesses;                 // Save expected probabilities
    fitnesses.resize(popPtr->size());
    double accumulatedFitness = 0.0;
    for (std::size_t i=0; i<popPtr->size(); i++) {
        fitnesses[i]       = popPtr->getIndividual(i)->fitness();
        accumulatedFitness += fitnesses[i];
    }
    std::ofstream f;           
    f.open("rouletteWheelSelection.probabilities.dat");
    for (auto el : fitnesses) {
        f << el/accumulatedFitness << "\n";
    }
    f.close();
    std::cout << "Saved file as rouletteWheelSelection.probabilities.dat to check statistics" << std::endl;
    f.open("rouletteWheelSelection.parentIndices.dat"); // Save parentIndices to evaluate statistics
    for (auto el : parentIndices) {
        f << el << "\n";
    }
    f.close();
    std::cout << "Saved file as rouletteWheelSelection.parentIndices.dat to check statistics" << std::endl;

}
