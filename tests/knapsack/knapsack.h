#include "../include/individual.h"
#include <vector>
#include <string>
#include <tuple>

using namespace std;

class Knapsack : public Genetics::Individual<bool> {
	
	public:	
	
		// Constructors
		Knapsack(int maxWeight, vector< tuple<string, int, int> >& data);
		//TODO: Destructor, ~Knapsack(); does this adhere to the interface or does it clash?
	
		// Member functions
		int getMaxWeight(void);
		void printContent(void);
				
		// Interface functions
        double fitness(void);
        size_t genomeLength(void);
        void setGene(int index, bool gene);
        bool getGene(int index);
		
	private:
	
		// Set at construction
		double maxWeight_;
		
		// Store the genotype
		vector<string> names_;
		vector<int> weights_;
		vector<int> values_;
		
		// Store genome
		vector<bool> genome_;
	
};
