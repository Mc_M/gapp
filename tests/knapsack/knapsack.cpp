#include "knapsack.h"
#include <cstddef>
#include <iostream>

//
// ========================= Constructors ==============================
//
Knapsack::Knapsack(int maxWeight, std::vector< std::tuple< std::string, int, int > >& data) {

	maxWeight_ = maxWeight;
	
	// Assign genotype
	names_.resize(data.size());
	weights_.resize(data.size());
	values_.resize(data.size());
	for(std::size_t ii=0; ii<data.size(); ii++) {
		names_[ii] = std::get<0>(data[ii]);
		weights_[ii] = std::get<1>(data[ii]);
		values_[ii] = std::get<2>(data[ii]);
	}
	
	// Initialize genome
	genome_.resize(data.size());
	for(std::size_t ii=0; ii<data.size(); ii++) {
		genome_[ii] = false;
	}
	
}

//
// ======================= Member functions ============================
//
int Knapsack::getMaxWeight(void) {
	return maxWeight_;
}

void Knapsack::printContent(void) {
	for(std::size_t ii=0; ii<genomeLength(); ii++) {
		std::cout << names_[ii] << "  " << weights_[ii] << "  " << values_[ii] << endl;
	}
}

//
// ======================= Member functions ============================
//

double Knapsack::fitness(void) {
	double result = 0.0;
	int w = 0;
	for (std::size_t ii=0; ii<genomeLength(); ii++) {
		if (genome_[ii]) {
			result += values_[ii];
			w      += weights_[ii];
		}
	}
	if (w > maxWeight_) {
		result = 0.0;
	}
	return result;
}

std::size_t Knapsack::genomeLength(void) {
	return genome_.size();
}

void Knapsack::setGene(int index, bool gene) {
	genome_[index] = gene;
}

bool Knapsack::getGene(int index) {
	return genome_[index];
}

//
// ========================== Direct test ==============================
//

// TODO: Put direct test to separate file.

int main() {
	
	// Raw data
	std::vector<int> values {150, 35, 200, 160, 60, 45, 60, 40, 30, 10, 70, 30,
							 15, 10, 40, 70, 75, 80, 20, 12, 50, 10, 1, 150};
	std::vector<int> weights {9, 13, 153, 50, 15, 68, 27, 39, 23, 52, 11,
							  32, 24, 48, 73, 42, 43, 22, 7, 18, 4, 30, 90,
						      200};
	std::vector< std::string > names {"map", "compass", "water", "sandwich",
									  "glucose", "tin", "banana", "apple",
									  "cheese", "beer", "suntan cream", "camera",
									  "T-shirt", "trousers", "umbrella", "waterproof trousers",
									  "waterproof overclothes", "note-case", "sunglasses", "towel",
									  "socks", "book", "notebook", "tent"};
	
	// Construct data
	std::vector< std::tuple< std::string, int, int > > data;
	data.resize(values.size());
	for (std::size_t ii=0; ii<values.size(); ii++) {
		data[ii] = std::make_tuple(names[ii], weights[ii], values[ii]);
	}
	
	// Knapsack object	
	int maxWeight = 500;
	Knapsack ks(maxWeight, data);
	
	// Test functions 1: maximal weight
	cout << "max weight: " << ks.getMaxWeight() << endl;
	
	// Test functions 2: Gene modifications
	int index = 10;
	cout << "gene index=10: " << ks.getGene(index) << endl;
	ks.setGene(index, true);
	cout << "gene index=10: " << ks.getGene(index) << endl;
	ks.setGene(index, false);
	cout << "gene index=10: " << ks.getGene(index) << endl;
	
	// Test functions 3: Check if data was passed by reference
	ks.printContent();
	names[index] = "HIHIHI";
	std::cout << std::endl << std::endl;
	ks.printContent();
	
	// Test functions 4: Fitness
	std::cout << "ks.fitness() = " << ks.fitness() << std::endl;
	ks.setGene(index, true);
	std::cout << "ks.fitness() = " << ks.fitness() << std::endl;
	
	return 0;
}
