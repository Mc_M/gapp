#include <iostream>
#include <vector>
#include <cmath>
#include <memory>
#include <cstdlib>
#include <string>
#include <sstream>
#include <fstream>
#include <random>
#include <cassert>

#include "../../include/equation.h"

int Equation::ctr = 0;

// Constructors
Equation::Equation(std::vector<int> x, std::vector<int> c, int rhs, bool verbose) {
    assert(x.size()==c.size() && "x and c must have same size.");
    x_       = x;
    c_       = c;
    rhs_     = rhs;
    ctr      += 1;
    id_      = ctr;
    verbose_ = verbose;
    if (verbose_) {
        std::cout << "Equation constructed." << std::endl;
    }
};

// Destructor
Equation::~Equation() {
    if (verbose_) {
        std::cout << "Equation destructed." << std::endl;
    }
}

// Function members
std::vector<int> Equation::getX() {
    return x_;
}

std::vector<int> Equation::getC() {
    return c_;
}

int Equation::getRhs() {
    return rhs_;
}

// ====================== Interface functions ==========================

double Equation::fitness() {
    // Compute fObjective
    float fObjective = 0.0;
    for (std::size_t i=0; i<genomeLength(); i++) {
            fObjective += c_[i]*x_[i];
    }
    fObjective -= rhs_;
    fObjective = std::abs(fObjective);
    if (verbose_) {
        std::cout << "fitness() called" << std::endl;
    }
    // Compute fitness
    return 1/(1+fObjective);
}

std::size_t Equation::genomeLength() {
    return c_.size();
}

void Equation::setGene(int index, int gene) {
    x_[index] = gene;
}

int Equation::getGene(int index) {
    return x_[index];
}

std::string Equation::print() {
    std::vector<int> x = getX();
    std::vector<int> c = getC();
    std::ostringstream res;
    res << "{" << id_ << "}: " << getRhs() << " == "
                << c[0] << " * ( " << x[0] << " )";
    for (std::size_t i=1; i<genomeLength(); i++) {
        res << " + " << c[i] << " * ( " << x[i] << " )";
    }
    return res.str();
}

// ======================== Factory functions ==========================

std::shared_ptr< Equation > Equation::getRandomEquationPtr(std::default_random_engine &gen,
                                                           std::uniform_int_distribution<> &dist,
                                                           std::vector<int> c, int rhs, bool verbose) {
    std::size_t gL = c.size();
    std::vector<int> x;
    x.resize(gL);
    for (std::size_t i=0; i<gL; i++) {
        x[i] = dist(gen);
    }
    return std::make_shared<Equation>(x, c, rhs, verbose);
}

// ======================== Infrastructural ==========================

std::ostream &operator<<(std::ostream &os, Equation &eq) {
    return os << eq.print();
}
