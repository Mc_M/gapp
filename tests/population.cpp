/**
 * @brief Test for class Population which is part of GApp.
 * 
 * It uses the previously tested class Equation that implements an Individual
 * to test the Population class.
 */

#include <random>
#include <memory>
#include "../include/equation.h"
#include "../include/population.h"

// =====================================================================
//                                 main
// =====================================================================

int main(void) {
    
    // Build population
    std::size_t cap = 6;
    std::size_t size = cap-2;
    GApp::Population<int> pop(cap, true);
    std::cout << "Check that Population is constructed at program start." << std::endl << std::endl;
    
    // Random generator
    std::default_random_engine r(0);
    std::size_t randomRange = 100;
    
    // Fitness buffer
    std::vector<std::pair<double, std::size_t>> fitnesses;
    fitnesses.resize(size);
    
    // Fill population
    int rhs = 42;                                                   // RHS
    std::vector<int> c;                                             // Coefficients
    std::size_t cSize = 4;
    c.resize(cSize);
    for (std::size_t i=0; i<cSize; i++) {
        c[i] = r() % (2*randomRange) - randomRange;
    }
    std::cout << "Equation instances to be added:" << std::endl;   // Add Equation objects
    for (std::size_t i=0; i<size; i++) {
        std::vector<int> x;
        x.resize(cSize);
        for (std::size_t j=0; j<cSize; j++) {
            x[j] = r() % (2*randomRange) - randomRange;
        }
        std::shared_ptr<Equation> eqPtr = std::make_shared<Equation>(x, c, rhs, false);
        pop.addIndividual(eqPtr);
        std::cout << " -> " << *eqPtr << std::endl;
        (fitnesses[i]).first = eqPtr->fitness();
        (fitnesses[i]).second = i+1; // This is equivalent
                                     // to the id of the
                                     // equation
    }
    std::cout << std::endl;
    
    // Check content
    std::cout << "Equation instances that are contained:" << std::endl;
    for (std::size_t i=0; i<size; i++) {
        std::cout << " -> " << pop.getIndividual(i)->print() << std::endl;
    }
    std::cout << std::endl;
    
    // Check fitnesses of original Equation objects
    std::cout << "Fitnesses of individuals:" << std::endl;
    for (auto el : fitnesses) {
        std::cout << " -> " << el.first << " " << el.second << std::endl;
    }
    std::cout << std::endl;
    
    // Check fitness sorting
    pop.updateRanking();
    std::cout << "All individuals sorted by their fitness:" << std::endl;
    for (std::size_t i=0; i<size; i++) {
        std::cout << " -> " << i << " -> " << pop.getFittestIndividual(i)->print() << std::endl;
    }
    std::cout << std::endl;
    
    // Check highest fitness to be fittest according to ordering
    std::cout << "Compare highest fitness against ordering:" << std::endl;
    std::cout << "         highestFitness(): " << pop.highestFitness() << std::endl;
    std::cout << "  getFittestIndividual(0): " << pop.getFittestIndividual(0)->fitness() << std::endl;
    std::cout << "                fitnesses: ";
    for (auto el : fitnesses) {
        std::cout << el.first << " ";
    }
    std::cout << std::endl << std::endl;
    
    // Check lowest fitness to be least fittest according to ordering
    std::cout << "Compare lowest fitness against ordering:" << std::endl;
    std::cout << "               lowestFitness(): " << pop.lowestFitness() << std::endl;
    std::cout << "  getFittestIndividual(size-1): " << pop.getFittestIndividual(pop.size()-1)->fitness() << std::endl;
    std::cout << "                     fitnesses: ";
    for (auto el : fitnesses) {
        std::cout << el.first << " ";
    }
    std::cout << std::endl << std::endl;
    
    // Check average fitness
    double fAvg = 0;
    for (auto el : fitnesses) {
        fAvg += el.first;
    }
    fAvg /= size;
    std::cout << "Average fitness:" << std::endl;
    std::cout << "      pop.averageFitness(): " << pop.averageFitness() << std::endl;
    std::cout << "                    manual: " << fAvg << std::endl;
    std::cout << "   iterate over population: ";
    fAvg = 0;
    for (std::size_t i=0; i<pop.size(); i++) {
        fAvg += pop.getIndividual(i)->fitness();
    }
    fAvg /= pop.size();
    std::cout << fAvg << std::endl << std::endl;
    
    // Population size and capacity
    std::cout << "Population size: " << pop.size() << " vs " << size << std::endl;
    std::cout << "Population capacity: " << pop.capacity() << " vs " << cap << std::endl;
    std::cout << std::endl;
    
    // Check updateRanking() and that only the full ones are sorted.
    std::cout << "Check updateRanking():" << std::endl;
    pop.updateRanking();
    for (auto el : fitnesses) {
        std::cout << " -> " << el.first << " & " << el.second-1 << " (id -> index)" << std::endl;
    }
    std::cout << std::endl;
    
    // Test addIndividual
    std::cout << "Check addIndividual():" << std::endl;
    std::size_t nrMoreEqns = cap-size+1;
    std::vector<std::shared_ptr<Equation>> moreEqns;
    moreEqns.resize(nrMoreEqns);
    for (std::size_t i=0; i<nrMoreEqns; i++) {
        std::vector<int> x;
        x.resize(cSize);
        for (std::size_t j=0; j<cSize; j++) {
            x[j] = r() % (2*randomRange) - randomRange;
        }
        std::shared_ptr<Equation> eqPtr = std::make_shared<Equation>(x, c, rhs, false);
        moreEqns[i] = eqPtr;
        std::cout << "  " << (moreEqns[i])->print() << std::endl;
    }
    for (auto el : moreEqns) {                      // Add to population. Last one should fail.
        int ret = pop.addIndividual(el);
        std::cout << "  " << ret << std::endl;
    }
    
    // Test getIndividual
    std::cout << "Check getIndividual():" << std::endl;
    for (std::size_t i=size; i<cap; i++) {
        std::cout << pop.getIndividual(i)->print() << std::endl;
    }
    
    // Test destructor
    std::cout << "Check that Population is destructed at program exit." << std::endl << std::endl;

}
