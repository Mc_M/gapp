CC=g++

# Folders
BIN=bin/
OBJECTS=objects/

# ================================== Tests =====================================

# ------------------------------- Individuals ----------------------------------
	
equation:
	# Folder structure
	mkdir -p $(OBJECTS)tests/individuals/
	# Compiling
	$(CC) -Wall -c -o $(OBJECTS)tests/individuals/equation.o -g tests/individuals/equation.cpp

# ---------------------------------- Tests -------------------------------------

equationTest: equation
	# Folder structure
	mkdir -p $(BIN)tests
	# Compile and link
	$(CC) -Wall -o $(BIN)tests/equation.x -g tests/equation.cpp $(OBJECTS)tests/individuals/equation.o
	
populationTest: equation
	# Folder structure
	mkdir -p $(BIN)tests
	# Compile and link
	$(CC) -Wall -o $(BIN)tests/population.x -g tests/population.cpp $(OBJECTS)tests/individuals/equation.o
	
mutationTest:
	# Folder structure
	mkdir -p $(BIN)tests
	# Compile and link
	$(CC) -Wall -o $(BIN)tests/mutation.x -g tests/mutation.cpp
	
crossoverTest:
	# Folder structure
	mkdir -p $(BIN)tests
	# Compile and link
	$(CC) -Wall -o $(BIN)tests/crossover.x -g tests/crossover.cpp
	
selectionTest: equation
	# Folder structure
	mkdir -p $(BIN)tests
	# Compile and link
	$(CC) -Wall -o $(BIN)tests/selection.x -g tests/selection.cpp $(OBJECTS)tests/individuals/equation.o

test: equation
	# Folder structure
	mkdir -p $(BIN)tests
	# Compile and link
	$(CC) -Wall -o $(BIN)tests/test.x -g tests/test.cpp $(OBJECTS)tests/individuals/equation.o

	
# ================================== Misc ======================================

clean:
	rm -rf $(BIN)
	rm -rf $(OBJECTS)
