// Individual
//
// Classes implementing this interface can be treated as individual that
// are used in Genetic Algorithms.

#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include <vector>
#include <iostream>
#include <string>

namespace GApp {

    /**
     * @class Individual
     * 
     * @brief Individual interface.
     * 
     * Interface that defines methods of an individual for genetic algorithm and
     * other algorithms.
     * 
     * @tparam GeneType The numeric type to represent a gene with.
     */
    template <class GeneType>
    class Individual {
            
        public:
                
            // Constructors
            //~ virtual Individual() // TODO: COPY CONSTRUCTOR MUST BE IMPLEMENTED
            
            /**
             * @brief Virtual destructor.
             */
            virtual ~Individual() {
                    // TODO: Print if debug flag is on. It could be implemented
                    //       by having a isDebug() function that defaults to false
                    //       but can be implemented.
                    //~ std::cout << "Destructor of Individual called!" << std::endl;
            };

            // ===================== Function members ==========================
            
            /**
             * @brief Fitness of individual.
             * 
             * The fitness computation is typically computationally expensive.
             * The value should be cached internally by the implementation of
             * this abstract method because it is called frequently by
             * GeneticAlgorithm and other algorithms.
             * 
             * @return The fitness assigned to this individual. The value can be
             * from the whole double range.
             */
            virtual double fitness() = 0;
            
            /**
             * @brief Length of genome.
             * 
             * @return The number of genes that is used to represent the genome
             * of this individual.
             */
            virtual std::size_t genomeLength() = 0;
            
            /**
             * @brief Set gene in genome.
             * 
             * @param index The index of the gene in the genome to set.
             * @param gene The value to which the gene is set.
             */
            virtual void setGene(int index, GeneType gene) = 0; // TODO: int -> std::size_t
            
            /**
             * @brief Get gene in genome.
             * 
             * @param index The index of the gene in the genome to get.
             * 
             * @return The value of the gene.
             */
            virtual GeneType getGene(int index) = 0; // TODO: int -> std::size_t
            
            /**
             * @brief String representation of individual.
             * 
             * @return Printable string representation.
             */
            virtual std::string print() = 0;
            
            // ===================== Derived functions =========================

            /**
             * @brief Complete genome of individual.
             * 
             * This is a wasteful implementation: A new vector is created and
             * filled using getGene() and genomeLength().
             * 
             * @return Sequence of single genes that represent the individual.
             */
            std::vector<GeneType> getGenome() {
                std::vector<GeneType> genome;
                genome.resize(genomeLength());
                for (std::size_t ii=0; ii<genomeLength(); ii++) {
                    genome[ii] = getGene(ii);
                }
                return genome;
            }
            
            /**
             * @brief Set complete genome of individual.
             * 
             * This is a fast implementation that uses setGene() and genomeLength().
             * 
             * @return Return code of operation: 0 if the genome was set and 1
             * otherwise.
             */
            int setGenome(std::vector<GeneType> &genome) {
                if (genome.size() == genomeLength()) {
                    for (std::size_t ii=0; ii<genomeLength(); ii++) {
                        setGene(ii, genome[ii]);
                    }
                    return 0;
                } else {
                    return 1;
                }
            }
                    
    };

}

#endif
