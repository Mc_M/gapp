// Population
//
// Class that represents a population of Individual objects.

#ifndef POPULATION_H
#define POPULATION_H

#include "individual.h"
#include <vector>
#include <algorithm>
#include <cassert>
#include <memory>

// ------------------------- Declarations ------------------------------

namespace GApp {

    /**
     * @class Population
     * 
     * @brief Population of individuals.
     * 
     * A population is a collection of individuals that implement the Individual
     * interface.
     * 
     * @tparam GeneType The numeric type to represent a gene with.
     */
    template <class GeneType>
    class Population {
            
        public:
                
            // ======================= Constructors ============================
            
            /**
             * @brief Constructor.
             * 
             * @param capacity The maximal number of individuals this Population
             * can store. Must be odd.
             * @param verbose Print internal details to stdout if true. Defaults
             * to false.
             */
            Population(std::size_t capacity, bool verbose=false);
            
            /**
             * @brief Destructor.
             */
            ~Population();
            
            // TODO: Add copy constructor to make a deep copy.
            
            // ====================== Function members =========================
            
            /**
             * @brief Size of population.
             * 
             * The current number of Individual instances stored in this
             * Population instance.
             */
            std::size_t size();
            
            /**
             * @brief Capacity of population.
             * 
             * Maximal number of individuals the Population instance can hold.
             */
            std::size_t capacity();
            
            /**
             * @brief Highest fitness of individuals in population.
             * 
             * The highest fitness values in this Population.
             */
            float highestFitness();
            
            /**
             * @brief Average fitness of individuals in population.
             * 
             * The average of the fitness value is taken over the Individual
             * objects stored currently in this Population.
             */
            float averageFitness();
            
            /**
             * @brief Lowest fitness of individuals in population.
             * 
             * The lowest fitness in this Population.
             */
            float lowestFitness();
            
            /**
             * @brief Update fitness ranking of individuals in population.
             * 
             * It must be called explicitly before any methods that rely on the
             * fitness ordering are invoked.
             */
            void updateRanking();
            
            /**
             * @brief Add an Individual instance to this population.
             * 
             * Add pointer of an Individual to this Population instance.
             * 
             * @param individual Smart pointer to Individual implementation.
             * 
             * @return Success code: 0 for success and 1 for failure due to
             * exceeded capacity ("not size < capacity").
             */
            int addIndividual(std::shared_ptr< Individual<GeneType> > individual);
            
            /**
             * @brief Return Individual stored in this population.
             * 
             * Return the smart pointer to an Invidivdual stored in this population.
             * 
             * @param indexIndividual Index to the Individual in this population.
             * It is zero-indexed and must be 0 <= index < size() which is
             * checked by an assert.
             * 
             * @return Smart pointer to the Individual.
             */
            std::shared_ptr< Individual<GeneType> > getIndividual(std::size_t indexIndividual);
            
            /**
             * @brief Return an Individual ordered by its fitness.
             * 
             * Return an Individual based on an index that takes the fitnesses
             * of all individuals into account. updateRanking() should be called
             * prior to this method.
             * 
             * @param indexIndividual The fitness-ordered index. Smaller indices
             * correspond to fitter individuals: 0 is the fittest and size-1 is
             * the worst. An assert tests the index to be valid.
             * 
             * @return Smart pointer to the Individual.
             */
            std::shared_ptr< Individual<GeneType> > getFittestIndividual(std::size_t indexIndividual);
        
        private:
        
            /**
             * @brief Verbose option.
             * 
             * Informational messages are printed to std::cout if true.
             */
            bool verbose_;
            
            /**
             * @brief Storage for capacity of population.
             */
            std::size_t capacity_;
            
            /**
             * @brief Storage for size of population.
             */
            std::size_t size_;
            
            /**
             * @brief Container to store Individual instances.
             * 
             * Underlying storage to save the pointers to the Individual instances
             * of this Population instance.
             */
            std::vector< std::shared_ptr< Individual<GeneType> > > individuals_;
            
            /**
             * @brief Ranking of Individual instances according to their fitnesses.
             * 
             * Mapping of fitness values to Individual indices in this Population
             * instance. This order enables easy sorting for the fitness values.
             * It will be sorted according to the fitness values.
             */
            std::vector< std::pair< double, std::size_t > > ranking_;
            
    };

}

// -------------------------- Definitions ------------------------------

template <class GeneType>
GApp::Population<GeneType>::Population(std::size_t capacity, bool verbose) {
    assert(capacity%2==0 && "Population size must be even.");
    capacity_ = capacity;
    size_ = 0;
    individuals_.resize(capacity_);
    ranking_.resize(capacity_);
    for (std::size_t i=0; i<capacity; i++) {
        ranking_[i].first = -1.0;
        ranking_[i].second = capacity;
    }
    verbose_ = verbose;
    if (verbose_) {
        std::cout << "Population created." << std::endl;
    }
}

template <class GeneType>
GApp::Population<GeneType>::~Population() {
    if (verbose_) {
        std::cout << "Population destroyed." << std::endl;
    }
}

template <class GeneType>
std::size_t GApp::Population<GeneType>::size() {
    return size_;
}

template <class GeneType>
std::size_t GApp::Population<GeneType>::capacity() {
    return capacity_;
}

template <class GeneType>
float GApp::Population<GeneType>::highestFitness() {
    if (size() != 0) {
        float fMax;
        float f;
        fMax = individuals_[0]->fitness();
        for(std::size_t i=1; i<size(); i++) {
            f = individuals_[i]->fitness();
            if (f > fMax) {
                fMax = f;
            }
        }
        return fMax;
    } else {
        return 0.0;
    }
}

template <class GeneType>
float GApp::Population<GeneType>::averageFitness() {
    if (size() != 0) {
        float f = 0.0;
        for(std::size_t i=0; i<size(); i++) {
            f += individuals_[i]->fitness();
        }
        return f/size();
    } else {
        return 0.0;
    }
}

template <class GeneType>
float GApp::Population<GeneType>::lowestFitness() {
    if (size() != 0) {
        float fMin;
        float f;
        fMin = individuals_[0]->fitness();
        for(std::size_t i=1; i<size(); i++) {
            f = individuals_[i]->fitness();
            if (f < fMin) {
                fMin = f;
            }
        }
        return fMin;
    } else {
        return 0.0;
    }
}

template <class GeneType>
int GApp::Population<GeneType>::addIndividual(std::shared_ptr< Individual<GeneType> > individual) {
    if (size() < capacity()) {
        individuals_[size()] = individual;
        size_ += 1;
        if (verbose_) {
            std::cout << "  Individual added." << std::endl;
        }
        return 0;
    } else {
        if (verbose_) {
            std::cout << "  Individual not added." << std::endl;
        }
        return 1;
    }
}

template <class GeneType>
std::shared_ptr< GApp::Individual<GeneType> > GApp::Population<GeneType>::getIndividual(std::size_t indexIndividual) {
    assert(indexIndividual>=0 and indexIndividual<size() && "Individual index not valid.");
    return individuals_[indexIndividual];
}

template <class GeneType>
std::shared_ptr< GApp::Individual<GeneType> > GApp::Population<GeneType>::getFittestIndividual(std::size_t indexIndividual) {
    assert(indexIndividual>=0 and indexIndividual<size() && "Individual index not valid.");
    return getIndividual((ranking_[indexIndividual]).second);
}

template <class GeneType>
void GApp::Population<GeneType>::updateRanking() {
    for (std::size_t i=0; i<size(); i++) {
        ranking_[i].first = getIndividual(i)->fitness();
        ranking_[i].second = i;
    }
    // Sorted starting from the fittest
    std::sort(ranking_.begin(), ranking_.begin()+size(), std::greater< std::pair< double, std::size_t > >());
    // Verbosity
    if (verbose_) {
        for (std::size_t i=0; i<capacity(); i++) {
            std::cout << "  " << ranking_[i].first << " & " << ranking_[i].second << std::endl;
        }
    }
}

#endif
