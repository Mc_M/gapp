// Mutation
//
// Static class that provides pre-implemented mutation operators.

#ifndef MUTATION_H
#define MUTATION_H

#include "geneticalgorithmsettings.h"
#include <vector>
#include <cstddef>
#include <random>
#include <cassert>

namespace GApp {
    
    /**
     * @class Mutation
     * 
     * @brief Pre-implemented mutation operators.
     * 
     * Implementations of useful mutation operators.
     */
    class Mutation {

        public:

            // ======================= Constructors ============================

            /**
             * \brief Constructor.
             */
            Mutation() {};

            /**
             * \brief Destructor.
             */
            ~Mutation() {};

            // ===================== Function members ==========================
            
            /**
             * @brief Integer Gaussian distribution.
             * 
             * A rounded and casted normal distribution is used. A binomial
             * distribution could be used according to "De Moivre–Laplace theorem"
             * but then the width would be hard to control.
             * 
             * @param value Mean of the distribution. The result is saved back
             * into this variable.
             * @param settings Its member width is used for the distribution width.
             * Also, its random number generator is used by the distribution.
             * 
             * @return The drawn random variable from distribution with mean at
             * value and settings.width is saved back into value.
             */
            static void intGaussian(int &value,
                                    GeneticAlgorithmSettings &settings);
            
            // Other possible operator implementations:
            // x) Bitflip
            // x) Normal distribution around old value

    };

}

// ========================= Implementations ===========================

void GApp::Mutation::intGaussian(int &value, GeneticAlgorithmSettings &settings) {
    std::normal_distribution<float> dist(value, settings.width);
    value = round(dist(settings.randomGenerator));
}

#endif
