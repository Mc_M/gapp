#ifndef EQUATION_H
#define EQUATION_H

#include <iostream>
#include <vector>
#include <cmath>
#include <memory>
#include <cstdlib>
#include <string>
#include <sstream>
#include <fstream>
#include <random>
#include "individual.h"

/**
 * @class Equation
 * 
 * @brief Equation int-individual.
 * 
 * A multivariate linear equation where the fitness is determined by its solution.
 * It is C * X = rhs where C and X are vectors of the same dimension and store the
 * constant coefficients and the proposed solution, respectively. The fitness is
 * 1 if the equation, characterized by C and rhs, is solved by X.
 */
class Equation : public GApp::Individual<int> {
	
    public:
        
        /**
         * @brief Equation counter.
         * 
         * It counts the number of created Equation instances.
         */
        static int ctr;
            
        // ========================== Constructors =============================
        
        /**
         * @brief Constructor.
         * 
         * @param x The proposed solution. Must be the same dimension as c.
         * @param c The constant equation parameters. Must be the same dimension
         * as x.
         * @param rhs The right-hand side of the equation.
         * @param verbose Print additional internal logic steps if true.
         * 
         * An assert is used to ensure that c and x have the same size.
         */
        Equation(std::vector<int> x, std::vector<int> c, int rhs, bool verbose);

        /**
         * @brief Destructor.
         */
        ~Equation();
        
        // ======================== Function members ===========================
        
        /**
         * @brief Get proposed solution.
         * 
         * Return the vector of numbers that are used as solution vector X.
         */
        std::vector<int> getX();
        
        /**
         * @brief Get constant equation parameters.
         * 
         * Return the vector of constant equation parameters C.
         */
        std::vector<int> getC();
        
        /**
         * @brief Get right-hand side of equation.
         */
        int getRhs();

        // ====================== Interface functions ==========================

        /**
         * @brief Fitness of Equation instance.
         * 
         * The fitness is computed as 1/(1+abs(C*X-rhs)). Thus, it is 1 if the
         * equation is solved by the solution but decreases otherwise.
         */
        double fitness();
        
        /**
         * @brief The genome length of the Equation.
         * 
         * The length of C and, consequently, X as well.
         */
        std::size_t genomeLength();
        
        /**
         * @brief Set one entry of the solution vector.
         * 
         * Set X[index] to gene.
         */
        void setGene(int index, int gene);
        
        /**
         * @brief Get one entry of the solution vector.
         * 
         * Get X[index].
         */
        int getGene(int index);
        
        /**
         * @brief String representation of this Equation instance.
         * 
         * It comprises of the ID, C, X and rhs as string.
         */
        std::string print();
        
        // ======================== Factory functions ==========================

        /**
         * @brief Return pointer to random Equation solution.
         * 
         * Return a smart-pointer to an Equation instance with prescribed C,
         * rhs and verbose values. The solutions X are randomly generated.
         * 
         * It is a factory method to conveniently generate random Equation solutions
         * that can be used to initialize a population.
         * 
         * @param gen Random generator engine.
         * @param dist Distribution to draw X from.
         * @param c Constant parameter vector.
         * @param rhs Right-hand side value.
         * @param verbose Verbose value.
         */
        static std::shared_ptr< Equation > getRandomEquationPtr(std::default_random_engine &gen,
                                                                std::uniform_int_distribution<> &dist,
                                                                std::vector<int> c, int rhs, bool verbose);
        
    private:
            
        /**
         * @brief Internal storage for X.
         */
        std::vector<int> x_;
        
        /**
         * @brief Internal storage for C.
         */
        std::vector<int> c_;
        
        /**
         * @brief Internal storage for rhs.
         */
        int rhs_;
        
        /**
         * @brief Internal storage for verbose.
         */
        bool verbose_;
        
        /**
         * @brief Internal storage for Equation instance ID.
         * 
         * It is taken from the Equation::ctr.
         */
        int id_;

};

/**
 * @brief Make Equation instance printable to stdout.
 * 
 * It uses Equation::print().
 */
std::ostream &operator<<(std::ostream &os, Equation &eq);

#endif
