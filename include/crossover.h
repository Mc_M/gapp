// Crossover
//
// Static class that provides pre-implemented crossover operators.

#ifndef CROSSOVER_H
#define CROSSOVER_H

#include "geneticalgorithmsettings.h"
#include <vector>
#include <cstddef>
#include <random>
#include <cassert>
#include <algorithm>
#include <iostream>

namespace GApp {
    
    /**
     * @class Crossover
     * 
     * @brief Pre-implemented crossover operators.
     * 
     * Implementations of useful crossover operators.
     */
    class Crossover {

        public:

            // ======================== Constructors ===========================
            
            /**
             * @brief Constructor.
             */
            Crossover() {};
            
            /**
             * @brief Destructor.
             */
            ~Crossover() {};

            // ====================== Function members =========================
            
            /**
             * @brief Crossover at n points in the genome.
             * 
             * @tparam GeneType The numeric type to represent a gene with.
             * @param genomeLength Number of genes.
             * @param genome1 First genome.
             * @param genome2 Second genome.
             * @param settings Settings for operation. settings.nPoint is used to
             * determine the number of cutting sites. settings.randomGenerator()
             * is used to get the random numbers. The genome sub intervals are
             * printed if settings.verbose==2.
             */
            template <class GeneType>
            static void npoint(std::size_t genomeLength,
                               std::vector<GeneType> &genome1,
                               std::vector<GeneType> &genome2,
                               GeneticAlgorithmSettings &settings);

    };

}

// ========================= Implementations ===========================

/**
 * settings.nPoint unique random numbers are generated and subsequently used as
 * boundaries for genome sub intervals. These are swapped which is achieved by
 * swapping all odd intervals. It is asserted that settings.nPoint<=genomeLength-1.
 */
template <class GeneType>
void GApp::Crossover::npoint(std::size_t genomeLength,
                             std::vector<GeneType> &genome1,
                             std::vector<GeneType> &genome2,
                             GeneticAlgorithmSettings &settings) {
    assert(settings.nPoint<=genomeLength-1 && "settings.nPoint must be <= genomeLength-1");
    std::size_t nPoint = settings.nPoint;
    // Create random intervals to swap in increasing order
    std::vector< std::size_t > boundaries; // The intervals based on the boundaries
                                           // are from [boundaries[i], boundaries[i+1]).
    boundaries.resize(nPoint+2);
    std::size_t val = 0;
    for (std::size_t i=0; i<nPoint; i++) {
        bool found = false;
        do {
            val = settings.randomGenerator() % (genomeLength-1)+1;  // between 1 and genomeLength-1
            found = false;
            for (std::size_t j=0; j<i; j++) {
                if (boundaries[j]==val) {
                    found = true;
                    break;
                }
            }
        } while (found);
        boundaries[i] = val; // Save unique value
    }
    boundaries[nPoint+1] = genomeLength; // Save boundary values
    boundaries[nPoint]   = 0;
    std::sort(boundaries.begin(), boundaries.end()); // Sort the interval boundaries
    if (settings.verbose==2) {
        std::cout << "GApp::Crossover::npoint(): boundaries:" << std::endl;
        for (std::size_t i=0; i<boundaries.size()-1; i++) {
            std::cout << "    " << boundaries[i] << " ... " << boundaries[i+1] << std::endl;
        }
        std::cout << std::endl;
    }
    // Swap sub genomes (Alternating between swapping and not swapping sub genomes
    // to avoid a complete swap)
    for (std::size_t i=0; i<boundaries.size()-1; i++) {
        if (i%2==1) {
            std::size_t iStart = boundaries[i];
            std::size_t iEnd = boundaries[i+1];
            for (std::size_t j=iStart; j<iEnd; j++) {
                std::swap(genome1[j], genome2[j]);
            }
        }
    }
}

#endif
