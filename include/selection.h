// Selection
//
// Static class that provides pre-implemented selection algorithms.

#ifndef SELECTION_H
#define SELECTION_H

#include "individual.h"
#include "population.h"
#include "geneticalgorithmsettings.h"
#include <vector>
#include <cstddef>
#include <random>
#include <cassert>
#include <iostream>

namespace GApp {

    /**
     * @class Selection
     *
     * @brief Pre-implemented selection operators.
     *
     * Implementations of useful selection operators.
     */
    class Selection {

        public:

            // ======================= Constructors ============================

            /**
             * \brief Constructor.
             */
            Selection() {};

            /**
             * \brief Destructor.
             */
            ~Selection() {};

            // ===================== Function members ==========================

            /**
             * \brief Tournament selection operator.
             *
             * Deterministic tournament selection. A tournament of individuals
             * is created and only the fittest is selected.
             *
             * An assert checks that numberOfParents == parentIndices.size().
             *
             * @param popPtr Pointer to a population of individuals.
             * @param numberOfParents The number of parents to be selected.
             * @param parentIndices The indices of the selected individuals.
             * @param settings Settings that are used, settings.tournamentSize,
             * tournamentSize.randomGenerator and tournamentSize.verbose.
             * @tparam GeneType The numeric type to represent a gene with.
             *
             * @return parentIndices is filled with the indices of the selected
             * individuals that are stored in the population pointed to by popPtr.
             *
             */
            // TODO: Make it stochastic as well
            template <class GeneType>
            static void tournamentSelection(std::shared_ptr< Population<GeneType> > popPtr,
                                            std::size_t numberOfParents,
                                            std::vector<std::size_t> &parentIndices,
                                            GeneticAlgorithmSettings &settings);

            /**
             * \brief Roulette wheel selection operator.
             *
             * Select individuals proportional to their fitness, p_i=f_i/sum_j f_j.
             * This is implemented using a Categorial distribution with
             * std::discrete_distribution. An alternative approach could be a
             * flat distribution for the accumulated probabilities.
             *
             * An assert checks that numberOfParents == parentIndices.size().
             *
             * @param popPtr Pointer to a population of individuals.
             * @param numberOfParents The number of parents to be selected.
             * @param parentIndices The indices of the selected individuals.
             * @param settings Settings that are used, tournamentSize.randomGenerator
             * and tournamentSize.verbose.
             * @tparam GeneType The numeric type to represent a gene with.
             *
             * @return parentIndices is filled with the indices of the selected
             * individuals that are stored in the population pointed to by popPtr.
             *
             */
            template <class GeneType>
            static void rouletteWheelSelection(std::shared_ptr< Population<GeneType> > popPtr,
                                               std::size_t numberOfParents,
                                               std::vector<std::size_t> &parentIndices,
                                               GeneticAlgorithmSettings &settings);

    };

}

// ========================= Implementations ===========================

template <class GeneType>
void GApp::Selection::tournamentSelection(std::shared_ptr< Population<GeneType> > popPtr,
                                          std::size_t numberOfParents,
                                          std::vector<std::size_t> &parentIndices,
                                          GeneticAlgorithmSettings &settings) {
    assert(numberOfParents == parentIndices.size() && "parentIndices is not big enough");
    for (std::size_t ii=0; ii<numberOfParents; ii++) {
        int chosenIndex = 0;
        for (std::size_t jj=0; jj<settings.tournamentSize; jj++) {
            int randomIndividualIndex = settings.randomGenerator() % popPtr->size();
            if (jj == 0) {
                chosenIndex = randomIndividualIndex;
            } else {
                if (  popPtr->getIndividual(randomIndividualIndex)->fitness()
                    > popPtr->getIndividual(      chosenIndex    )->fitness()) {
                    chosenIndex = randomIndividualIndex;
                }
            }
            if (settings.verbose==2) {
                std::cout << "    Random individual: "
                          << randomIndividualIndex << " -> "
                          << popPtr->getIndividual(randomIndividualIndex)->fitness()
                          << std::endl;
            }
        }
        parentIndices[ii] = chosenIndex;
        if (settings.verbose==2) {
            std::cout << "    Chosen (" << ii << "): " << parentIndices[ii] << std::endl;
        }
    }
    if (settings.verbose==2) {
        std::cout << std::endl << std::endl;
    }
}

template <class GeneType>
void GApp::Selection::rouletteWheelSelection(std::shared_ptr< Population<GeneType> > popPtr,
                                             std::size_t numberOfParents,
                                             std::vector<std::size_t> &parentIndices,
                                             GeneticAlgorithmSettings &settings) {
    assert(numberOfParents == parentIndices.size() && "parentIndices is not big enough");
    // Compute unnormalized propabilities
    std::vector<double> propabilities;
    propabilities.resize(popPtr->size());
    for (std::size_t i=0; i<popPtr->size(); i++) {
        propabilities[i] = popPtr->getIndividual(i)->fitness();
    }
    if (settings.verbose==2) {
        std::cout << std::endl;
        double accumulatedPropability = 0.0;
        for (auto p : propabilities) {
            accumulatedPropability += p;
        }
        std::cout << "Propabilities:" << std::endl;
        for (auto p : propabilities) {
            std::cout << "   " << p/accumulatedPropability << std::endl;
        }
        std::cout << std::endl;
    }
    // Build Categorial distribution
    std::discrete_distribution<> Categorial(propabilities.begin(), propabilities.end());
    // Select parents
    for (std::size_t ii=0; ii<numberOfParents; ii++) {
        parentIndices[ii] = Categorial(settings.randomGenerator);
        if (settings.verbose==2) {
            std::cout << "    Chosen (" << ii << "): " << parentIndices[ii] << std::endl;
        }
    }
    if (settings.verbose==2) {
        std::cout << std::endl << std::endl;
    }
}

#endif
