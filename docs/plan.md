# Plan

1. Develop a generic (enough) Genetic Algorithm.
2. Benchmark it with:
    1. Knapsack
    2. Travel and salesman
3. Then program the structure for the frogger neuroevolution
   project.

The benchmarks ensure a properly working GA and developing the GA
at first ensures that the frogger implementation fits in what is
required.
