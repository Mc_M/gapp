# Literature

* https://www.youtube.com/watch?v=BBLJFYr7zB8
* https://www.youtube.com/watch?v=aeWmdojEJf0
* http://www.scholarpedia.org/article/Neuroevolution
* "Neuroevolution" on Wikipedia
* http://code-spot.co.za/2009/10/08/15-steps-to-implemented-a-neural-net/
* Google: "neural network c++ tutorial"
* https://www.youtube.com/watch?v=KkwX7FkLfug

## Genetic algorithms

* https://stackoverflow.com/questions/3806469/bit-array-in-c
* http://www.cplusplus.com/forum/beginner/121180/
* https://stackoverflow.com/questions/20024690/is-there-byte-data-type-in-c
* https://www.google.co.uk/search?client=ubuntu&channel=fs&q=c%2B%2B+byte&ie=utf-8&oe=utf-8&gfe_rd=cr&dcr=0&ei=Z539WaDgMIT98wfVtbXIAg
* http://www.theprojectspot.com/tutorial-post/creating-a-genetic-algorithm-for-beginners/3
* https://arxiv.org/pdf/1308.4675.pdf

* http://www.nils-haldenwang.de/computer-science/computational-intelligence/genetic-algorithm-vs-0-1-knapsack
* Google: Knapsack problem example
* https://www.tutorialspoint.com/genetic_algorithms/index.htm
* http://www.obitko.com/tutorials/genetic-algorithms/crossover-mutation.php

## C++

* http://www.cs.dartmouth.edu/~campbell/cs50/buildlib.html.
* Really good on linking: https://www3.ntu.edu.sg/home/ehchua/programming/cpp/gcc_make.html#zz-1.3
* Cool tools: nm and objdump for studying the names inside an object file *.o

## NN

* Google: "neural network with python"
* https://iamtrask.github.io/
* http://www.wildml.com/2015/09/implementing-a-neural-network-from-scratch/
